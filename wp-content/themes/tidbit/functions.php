<?php
/**
 * tidbit functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package tidbit
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'tidbit_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tidbit_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on tidbit, use a find and replace
		 * to change 'tidbit' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'tidbit', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'header-menu' => esc_html__( 'Header', 'tidbit' )
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'tidbit_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'tidbit_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tidbit_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tidbit_content_width', 640 );
}
add_action( 'after_setup_theme', 'tidbit_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function tidbit_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'tidbit' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'tidbit' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'tidbit_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tidbit_scripts() {
	wp_enqueue_style( 'tidbit-style', get_stylesheet_uri(), array('tidbit-googlefonts'), _S_VERSION );
    wp_enqueue_style( 'tidbit-googlefonts', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Open+Sans:wght@300;400;600;700;800&display=swap');

    wp_style_add_data( 'tidbit-style', 'rtl', 'replace' );

    wp_enqueue_script('tidbit-script', get_template_directory_uri() . '/js/script.js');
    wp_enqueue_script('tidbit-script', get_template_directory_uri() .'/js/my-custom-script.js', array('jquery'), null, true);

    //====ad to cart
    wp_enqueue_script('ajax-ad-to-cart' , get_template_directory_uri() . '/js/ajax-ad-to-cart.js', array('jquery'), null, true);
    wp_localize_script('ajax-ad-to-cart', 'ad-to-cart_form' , array(
        'url' => admin_url( 'admin-ajax.php' ),
        'nonce' => wp_create_nonce('ad-to-cart-nonce')
    ));

    //end====

	wp_enqueue_script( 'tidbit-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tidbit_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//added

add_action( 'wp_enqueue_scripts', 'enqueue_load_fa' );
function enqueue_load_fa() {

    wp_enqueue_style( 'load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );
}

function fw_print ($expresion){
    echo'<pre class="fw_print">';
    var_dump($expresion);
    echo'</pre>';
}

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => __('Theme Settings', 'ganja'),
    ));

}

//==ПАГИНАЦИЯ ==================================================//
//Убираем комментарий к пагинации////

add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
function my_navigation_template( $template, $class ){

    return '
	<nav class="navigation %1$s" role="navigation">
		<div class="nav-links">%3$s</div>
	</nav>    
	';
}

// выводим пагинацию
the_posts_pagination( array(
    'end_size' => 2,
) );

///====КОНЕЦ==========================================================//

///===Подключаем шорт-код==============================================//
function my_custom_shortcode(){
    return '<h1> Ny text </h1>';

}

add_shortcode('test' , 'my_custom_shortcode') ;


add_action( 'login_enqueue_scripts', 'wpspec_custom_login_logo' );

function wpspec_custom_login_logo() {
    ?>
    <style>
        body.login h1 a {
            background: url('./image/logo.jpg') no-repeat;
            width: 100px;
            height: 100px;
        }
    </style>
    <?php
}
//================================================================================//

//WooCommerce функции и хуки//

function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

/**
 *  Load WooCommerce compatibility file.
 */
require get_template_directory() . '/woocommerce/includes/wc-functions.php';
require get_template_directory() . '/woocommerce/includes/wc-functions-remove.php';
require get_template_directory() . '/woocommerce/includes/wc-functions-cart.php';
require get_template_directory() . '/woocommerce/includes/wc-functions-single.php';
require get_template_directory() . '/woocommerce/includes/wc-function-archive.php';


