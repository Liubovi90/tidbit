<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
////add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
//remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
//remove_all_filters( 'woocommerce_after_single_product_summary');
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );




// убираем завершающие нули в ценах.
add_filter( 'woocommerce_price_trim_zeros', 'wc_hide_trailing_zeros', 10, 1 );
function wc_hide_trailing_zeros( $trim ) {

    return true;

}
// Меняем количество товара в одной строке в архиве продуктов на 3

add_filter( 'loop_shop_columns', 'wc_loop_shop_columns', 1, 10 );

/*
 * Return a new number of maximum columns for shop archives
 * @param int Original value
 * @return int New number of columns
 */
function wc_loop_shop_columns( $number_columns ) {
    return 3;
}
//даляем отзывы на странице товарв
add_filter( 'woocommerce_product_tabs', 'my_woo_remove_reviews_tab', 98 );
function my_woo_remove_reviews_tab($tabs) {
    unset($tabs['reviews']);
    return $tabs;
}

// удаляем title категории на странице
add_filter('woocommerce_show_page_title', '__return_false');

// =========================================================================
//REMOVE SIDEBAR OF GENESIS CHECKOUT AND CART PAGE
// =========================================================================
function remove_checkout_sidebar_genesis($opt) {
    if( is_checkout() || is_cart()) {
        $opt = 'full-width-content';
        return $opt;
    }
}
add_filter('genesis_site_layout', 'remove_checkout_sidebar_genesis');