<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
add_action( 'woocommerce_before_main_content', 'tidbit_archive_wrapper_start', 40 );
function tidbit_archive_wrapper_start(){
    ?>

    <div class="container">
    <?php
}

add_filter('woocommerce_show_page_title' , 'tidbit_hide_title_shop');
function tidbit_hide_title_shop($hide) {
    if ( is_shop()){
        $hide = false;
    }

    return $hide;
}
