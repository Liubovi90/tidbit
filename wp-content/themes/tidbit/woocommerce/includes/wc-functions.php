<?php
add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );

function custom_override_default_address_fields( $address_fields )
{
    $address_fields['postcode']['required'] = false;
    return $address_fields;
}
//add_action('woocommerce_after_checkout_validation', 'my_custom_checkout_field_process');



?>