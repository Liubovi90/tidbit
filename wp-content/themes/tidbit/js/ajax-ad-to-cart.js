jQuery(function ($) {
    if ($('.add_to_cart').on('click', function (e) {
        e.preventDefault();
        var data = $(this).data();
        $.ajax({
            type: "POST",
            url: data.link,
            data: {'quantity': data.qty, 'add-to-cart': data.id},
            success: function () {
                var count  = parseInt($('.cart-contents .count').text());
                $('.cart-contents .count').text(count + 1);
            }
        })
    })) ;
})