<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tidbit
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head();
    $main_logo = get_field('logo_global', 'option');
	?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div class="header " id="navbar">
    <div class="container">

        <div class="header__body">

            <div class="hamburger-menu">
                <input id="menu__toggle" type="checkbox" />
                <label class="menu__btn" for="menu__toggle">
                    <span></span>
                </label>
                <ul class="menu__box">
                    <li><?php
                        wp_nav_menu(array(
                            'theme_location' => 'header-menu',
                            'menu_id' => 'menu__box',
                            'menu_class' => 'menu__item',
                            'container'       => false,
                        ))
                        ?></li>
                </ul>
            </div>

            <div class="header__logo">
                <a  href="<?php echo home_url('/');?>"><img src="<?php echo $main_logo ['url']; ?>" alt="img" ></a>
            </div>

            <div class="header__menu">
                <ul class="header__list">
                    <li >
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'header-menu',
                            'menu_id' => 'menu_nav',
                            'menu_class' => 'header__list',
                            'container'       => false,
                        ))
                        ?>
                    </li>
                </ul>


            </div>

            <div class="header__icon">
                <a href="#"><?php woocommerce_cart_link();?></a>
                <div id="myDIV" class="search_blok"><?php get_search_form(); ?></div>
                <i class="fa fa-search " onclick="search_header()">
                </i>
            </div>

        </div>
    </div>
</div>

<script>
    var search = document.getElementById("myDIV");
    search.style.display = "none";
    function search_header() {
        if (search.style.display === "none") {
            search.style.display = "block";
        } else {
            search.style.display = "none";
        }
    }
</script>