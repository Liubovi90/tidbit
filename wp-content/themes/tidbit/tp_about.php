<?php
/*
Template Name: Подписка
*/
?>

<?php get_header(); ?>

<?php
$args =array(
    'post_type' => 'product',
   'product_cat' => 'subscription_set',
);
$query = new WP_Query( $args );
$posts = $query->posts;



$args =array(
    'post_type' => 'product',
    'product_cat' => 'subscription_package',
);
$query = new WP_Query( $args );
$posts_2 = $query->posts;


$args =array(
    'post_type' => 'product',
    'product_cat' => 'long_time',
);
$query = new WP_Query( $args );
$posts_3 = $query->posts;

global $woocommerce;
?>

<section class="advantage">
    <div class="advantage_box">
        <div class="container">
            <h1 class="title title_advantage">Ореховые пасты для себя</h1>
            <p class="advantage__text">Полезный десерт - всегда у вас дома</p>

            <div class="advantage__iconblock">

                <div class="advantage__item">
                    <div class="advantage__icon">
                        <i class="fa fa-truck"></i>
                    </div>
                    <p>Экономия времени</p>
                </div>

                <div class="advantage__item">
                    <div class="advantage__icon">
                        <i class="fa fa-truck"></i>
                    </div>
                    <p>Выгодная стоимость</p>
                </div>

                <div class="advantage__item">
                    <div class="advantage__icon">
                        <i class="fa fa-truck"></i>
                    </div>
                    <p>Бесплатная доставка</p>
                </div>

            </div>
        </div>
    </div>
    <div class="hero__bg advantage_bg"></div>
</section>

<section class="how">
    <div class="container">
        <div class="how__block">
            <div class="how__left">
                <div class="left_top">
                    <h2>Как это работает</h2>
                    <p>Прелести оформления подписка</p>
                </div>
                <div class="left_low">
                    <div class="left_low_text">
                        <h3>Наслаждайся</h3>
                        <p> полезным десертом, не задумываясь, что скоро придется ждать очередной радости>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="how__right">

                <div class="right_block" >
                    <h3 class="right_block_number">01</h3>
                    <p class="right_text">Выбери желаемый набор ореховых паст</p>
<!--                    <i class="fa fa-arrow-circle-up right_block_icon"></i>-->
                    <img src="/wp-content/themes/tidbit/image/first_how_icon.png" alt="image" class="right_block_image">
                    <div class="right_description">
                        <hr>
                        <p>Попробовав наши ореховые пасты однаждыб Вы навсегда поймете, что именно это Вам и не хватало
                            для получения удовольствия.</p>
                    </div>
                </div>

                <div class="right_block" >
                    <h3 class="right_block_number">02</h3>
                    <p class="right_text">Оформи и оплати заказ</p>
                    <!--                    <i class="fa fa-arrow-circle-up right_block_icon"></i>-->
                    <img src="/wp-content/themes/tidbit/image/second_how_icon.png" alt="image" class="right_block_image">
                    <div class="right_description">
                        <hr>
                        <p>Попробовав наши ореховые пасты однаждыб Вы навсегда поймете, что именно это Вам и не хватало
                            для получения удовольствия.</p>
                    </div>
                </div>

                <div class="right_block" >
                    <h3 class="right_block_number">03</h3>
                    <p class="right_text">Получи ореховые пасты курьером или по почте</p>
                    <!--                    <i class="fa fa-arrow-circle-up right_block_icon"></i>-->
                    <img src="/wp-content/themes/tidbit/image/third_how_icon.png" alt="image" class="right_block_image">
                    <div class="right_description">
                        <hr>
                        <p>Попробовав наши ореховые пасты однаждыб Вы навсегда поймете, что именно это Вам и не хватало
                            для получения удовольствия.</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="how__low">
            <div class="low_left left_item">
                <p>Будем выполнять ваши пожелания, чтобы ваш кухонный шкаф был полон</p>
            </div>
            <div class="low_center left_item">
                <img src="/wp-content/themes/tidbit/image/people.png" alt="image">
                <p>А в это время мы</p>
            </div>
            <div class="low_right left_item">
                <p>Напоминать, что пора продлить подписку на запасы орехового наслаждения</p>
            </div>
        </div>
    </div>
</section>

<?php if ($query->have_posts()){ ?>
<section class="registration">
    <div class="container">
        <h2>Оформление подписки</h2>
        <hr>
        <div class="packets">

            <div class="packets__item">
                <h4 class="packet__title">START</h4>
                <hr>
                <ul class="packet_list">
                    <li>
                        <i class="fa fa-check"></i>
                        Доставка 1 раз/месяц
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        Оплата при получении
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        Скидка -15 % на пасты
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        Доставка бесплатно по городу (пригород.районы от 70 лей)
                    </li>
                </ul>

                <div class="registration__set">
                    <?php foreach(($posts) as $post){
                        $product = new WC_Product($post->ID);
                        ?>
                        <div class="registration__item">

                            <div class="registrarion__item_image">
                                <a href="<?php the_permalink($product->get_id()); ?>"><?php echo get_the_post_thumbnail( $post->ID, 'thumbnail'); ?></a>
                            </div>
                            <p class="registration__item_title"><?php echo $product->get_title(); ?></p>
                            <p class="registration__item_text"><?php echo $product->get_description(); ?></p>
                            <div class="registration__item_price">
                                <p><?php echo $product->get_price_html(); ?></p>
                            </div>
                            <a data-link="<?php the_permalink($product->get_id()) ?>" data-qty="1" data-id="<?= $product->get_id()?>" class="button special__button add_to_cart registration__item_btn">Оформить подписку</a>
                        </div>
                    <?php }

                    wp_reset_postdata(); ?>
                </div>

            </div>

            <div class="packets__item">
                <h4 class="packet__title">I need More</h4>
                <hr>
                <ul class="packet_list">
                    <li>
                        <i class="fa fa-check"></i>
                        Доставка 2 раз/месяц
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        Оплата при получении
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        <span>Скидка -20 % на пасты</span>
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        Бесплатная доставка по Кишиневу
                    </li>

                </ul>

                <div class="registration__set">
                    <?php foreach(($posts_2) as $post){
                        $product = new WC_Product($post->ID);
                        ?>
                        <div class="registration__item">

                            <div class="registrarion__item_image">
                                <a href="<?php the_permalink($product->get_id()); ?>"><?php echo get_the_post_thumbnail( $post->ID, 'thumbnail'); ?></a>
                            </div>
                            <p class="registration__item_title"><?php echo $product->get_title(); ?></p>
                            <p class="registration__item_text"><?php echo $product->get_description(); ?></p>
                            <div class="registration__item_price">
                                <p><?php echo $product->get_price_html(); ?></p>
                            </div>
                            <a data-link="<?php the_permalink($product->get_id()) ?>" data-qty="1" data-id="<?= $product->get_id()?>" class="button special__button add_to_cart registration__item_btn">Оформить подписку</a>
                        </div>

                    <?php }

                    wp_reset_postdata(); ?>
                </div>
            </div>

            <div class="packets__item">
                <h4 class="packet__title">Long Time</h4>
                <hr>
                <ul class="packet_list">
                    <li>
                        <i class="fa fa-check"></i>
                        Доставка 1 раз/месяц в течении 3-х месяцев
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        Оплата при получении
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                       <span>Скидка -25 % на пасты</span>
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        Бесплатная доставка по Кишиневу
                    </li>

                </ul>

                <div class="registration__set">
                    <?php foreach(($posts_3) as $post){
                        $product = new WC_Product($post->ID);
                        ?>
                        <div class="registration__item">

                            <div class="registrarion__item_image">
                                <a href="<?php the_permalink($product->get_id()); ?>"><?php echo get_the_post_thumbnail( $post->ID, 'thumbnail'); ?></a>
                            </div>
                            <p class="registration__item_title"><?php echo $product->get_title(); ?></p>
                            <p class="registration__item_text"><?php echo $product->get_description(); ?></p>
                            <div class="registration__item_price">
                                <p><?php echo $product->get_price_html(); ?></p>
                            </div>
                            <a data-link="<?php the_permalink($product->get_id()) ?>" data-qty="1" data-id="<?= $product->get_id()?>" class="button special__button add_to_cart registration__item_btn">Оформить подписку</a>
                        </div>

                    <?php }

                    wp_reset_postdata(); ?>
                </div>
            </div>
        </div>



    </div>
</section>
<?php } ?>



<script>
    let rightBlock = document.querySelectorAll('.right_block');

       rightBlock.forEach((block) =>{

               block.onmouseover = function (e) {
                   block.querySelector('.right_block_number').style.top = '-100%';
                   block.querySelector('.right_block_image').style.top = '100%';
                   block.querySelector('.right_description').style.left = '0%';
               };
               block.onmouseout = function (e) {
                   block.querySelector('.right_block_number').style.top = '0%'
                   block.querySelector('.right_block_image').style.top = '60%';
                   block.querySelector('.right_description').style.left = '-100%';
               };

       });

    rightBlock.forEach((block) =>{

            block.onmouseover = function (e) {
                    block.querySelector('.right_block_number').style.top = '-100%';
                    block.querySelector('.right_block_image').style.top = '100%';
                    block.querySelector('.right_description').style.left = '0%';
            };
            block.onmouseout = function (e) {
                block.querySelector('.right_block_number').style.top = '0%'
                block.querySelector('.right_block_image').style.top = '60%';
                block.querySelector('.right_description').style.left = '-100%';
            };


    });
   
</script>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>

<?php get_footer() ?>
