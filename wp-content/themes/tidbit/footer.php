<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tidbit
 */

?>

<?php
$tel = get_field('telephone', 'option');
$country = get_field('country', 'option');
$adress = get_field('adress', 'option');
$email = get_field('email_footer', 'option');
$fb = get_field('facebook_link', 'option');
$insta = get_field('instagram_link', 'option');
//$twitter = get_field('twitter_link', 'option');

?>

<footer class="footer" id="contacts">
    <div class="container">
        <div class="footer__block">

            <div class="footer__adress">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <div class="footer__text">
                    <p><?php echo $country; ?></p>
                    <p><?php echo $adress; ?></p>
                </div>
            </div>

            <div class="footer__contact">
                <p><?php echo $tel; ?></p>
                <p><?php echo $email; ?></p>
            </div>

            <div class="footer__icon">
                <a href=""<?php echo $fb; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="<?php echo $insta; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
<!--                <a href="--><?php //echo $twitter; ?><!--"><i class="fa fa-twitter" aria-hidden="true"></i></a>-->
            </div>
        </div>
    </div>
</footer>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
