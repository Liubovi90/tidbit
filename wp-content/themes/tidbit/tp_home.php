<?php
/*
Template Name: Главная
*/
?>

<?php get_header(); ?>
<?php
$image_bg1= get_field('image_background_1');
$fb = get_field('facebook_link', 'option');
$insta = get_field('instagram_link', 'option');
//$twitter = get_field('twitter_link', 'option');
$image_autor= get_field('image_section_4');
$repeater_1 = get_field('repeater_section_2');

$args =array(
    'post_type' => 'product',
    'posts_per_page' => 3,
    'product_cat' => 'special-price',
);
$query = new WP_Query( $args );
$posts = $query->posts;
global $woocommerce;

?>

<section class="hero">


    <div class="hero__box" style="background-image: url(<?php echo $image_bg1['url']; ?>)">
        <div class="container">
            <div class="hero__block">
                <div class="hero__text">
                    <h1><?php the_field('title_hero')?></h1>

                </div>

                <div class="hero__social">
                    <div class="hero__social_block">
                        <div class="hero__icon">
                            <a href="<?php echo $fb; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="<?php echo $insta; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
<!--                            <a href="--><?php //echo $twitter; ?><!--"><i class="fa fa-twitter" aria-hidden="true"></i></a>-->
                        </div>
                        <div class="hero__content">
                            <div class="hero__line"></div>
                            <p>Social</p>
                        </div>

                    </div>

                </div>
            </div>


        </div>
    </div>
    <div class="hero__bg"></div>
    <!--    </div>-->
</section>

<section class="intro">
    <div class="container">
        <h1 class="title"><?php the_field('title_section_2')?></h1>
        <div class="intro__block">

            <?php foreach ($repeater_1 as $key => $post_1) { ?>
            <div class="intro__item"  style="background-image: url(<?php echo $post_1['image_sec2_repeater']['url'] ?>)">

                <div class="intro__button">
                    <a class="button" href="<?php echo $post_1 ['link_category_sec2']?>"><?php echo $post_1 ['button_section2_repeater']?></a>
                </div>

            </div>
            <?php }
            ?>

        </div>
    </div>
</section>

<?php if ($query->have_posts()){ ?>
<section class="special">
    <div class="container">
        <h1 class="title"><?php the_field('title_section_3')?></h1>

        <div class="special__block">
            <?php foreach(($posts) as $post){
            $product = new WC_Product($post->ID);
//
            ?>
            <div class="special__item">
                <?php echo get_the_post_thumbnail( $post->ID, 'thumbnail'); ?>
                <a class="special__item_title" href="<?php the_permalink($product->get_id()); ?>"><?php echo $product->get_title(); ?></a>
                <p class="special__text"><?php echo $product->get_description(); ?></p>
                <div class="special__price">
                    <p><?php echo $product->get_price_html(); ?></p>
                </div>
                <a data-link="<?php the_permalink($product->get_id()) ?>" data-qty="1" data-id="<?= $product->get_id()?>" class="button special__button add_to_cart">Добавить в корзину</a>
            </div>

            <?php }

            wp_reset_postdata(); ?>


        </div>
    </div>

</section>
<?php } ?>


<section class="about" id="about">
    <div class="container">
        <div class="about__block">
            <div class="about__image">
                <img src="<?php echo $image_autor['url']; ?>" alt="image">
            </div>
            <div class="about__content">
                <h2 class="title"><?php the_field('title_hero')?></h2>
                <p><?php the_field('text_section_4')?></p>
                <br>
                <span>Почему вы выбираете нас:</span>
                <ul class="about__list">
                    <li>10 из 10 говорят «ваши вкуснее»</li>
                    <li>Владельцы полезной сгущёнки и шокопасты</li>
                    <li>Для тех, кому не всё равно</li>
                    <li>Organic • Vegan • Premium</li>
                </ul>
            </div>
        </div>
    </div>
</section>



<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>

<?php get_footer() ?>
