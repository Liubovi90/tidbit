<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tidbit_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('WPLANG', 'ru_RU');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p^YqlpRTq@?Y(AB+fLDg3pcARn.r IUl-v#IK*m4DWB6z<LV:!2icI^/hSpM)[I*' );
define( 'SECURE_AUTH_KEY',  '!vG+PmX02Z2&ODAsW(I7PvXQsamtTmANCk91H=/#s;(bIV;SBkaofL=APIR)Q4i7' );
define( 'LOGGED_IN_KEY',    'tI}xK/X:Ra40E:jnQ*j(7$bGnjpu?(ZTLod T-QGze]5##zG{$u7%J%r9vJZbGPn' );
define( 'NONCE_KEY',        'f?~Uph)-}+#UNU`hsJw}eTUDO9CzG4M@~9,!<m48Me*zBMfT[hC2p7FLgUgj#?Ll' );
define( 'AUTH_SALT',        'd7>~TD)34R2p[F~jP0>*x%IV2e,d)s1n4Ttco$df!F<xZ-#fqYk_kIC!-?[XKLa?' );
define( 'SECURE_AUTH_SALT', 'f0}xX+c2(uT@n]xfBmGZlpf[$]H:Z}c$*/`#w|};`FJ,!5LGzTLaiHEx{= wb<(.' );
define( 'LOGGED_IN_SALT',   'tutTFkXnWDA]2^N;qGXaq0U`~4#;-8mb9$G{ ZDE^i%iailzAStGLi!Z+&#NT6X_' );
define( 'NONCE_SALT',       '9|D+ORyQ+WVjJV/XO,*]~p8&Q[cJF!~hassy5<>Xmi5BwSinFO8e:(Uack61S3E3' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';


